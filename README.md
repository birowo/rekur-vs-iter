referensi yang sangat jelas menurut saya:

https://faun.pub/implementing-recursive-and-iterative-dfs-on-a-binary-tree-golang-eda04949f4ee

![rekur_vs_iter1](https://gist.github.com/assets/21541959/32f3f78a-9f65-4980-8758-d37640f52ffd)

![rekur_vs_iter2](https://gist.github.com/assets/21541959/b88c16c8-5e01-4151-a08a-471890cc9134)