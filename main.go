package main

import "fmt"

type BinTree[T any] struct {
	Value       T
	Left, Right *BinTree[T]
}

func Rekursif[T any](t *BinTree[T], f func(T)) {
	f(t.Value)
	if t.Left != nil {
		Rekursif(t.Left, f)
	}
	if t.Right != nil {
		Rekursif(t.Right, f)
	}
}

func Iteratif[T any](t *BinTree[T], f func(T)) {
	stack := [...]*BinTree[T]{t, 63: nil} //max. tree height: 63
	var last *BinTree[T]
	i := 0
	for i != -1 {
		last = stack[i]
		f(last.Value)
		if last.Right != nil {
			stack[i] = last.Right
			i++
		}
		if last.Left != nil {
			stack[i] = last.Left
			i++
		}
		i--
	}
}

type B = BinTree[int]

var t = &B{1,
	&B{2,
		&B{4,
			&B{8,
				&B{16, nil, nil},
				&B{17, nil, nil},
			},
			&B{9,
				&B{18, nil, nil},
				&B{19, nil, nil},
			},
		},
		&B{5,
			&B{10,
				&B{20, nil, nil},
				&B{21, nil, nil},
			},
			&B{11,
				&B{22, nil, nil},
				&B{23, nil, nil},
			},
		},
	},
	&B{3,
		&B{6,
			&B{12,
				&B{24, nil, nil},
				&B{25, nil, nil},
			},
			&B{13,
				&B{26, nil, nil},
				&B{27, nil, nil},
			},
		},
		&B{7,
			&B{14,
				&B{28, nil, nil},
				&B{29, nil, nil},
			},
			&B{15,
				&B{30, nil, nil},
				&B{31, nil, nil},
			},
		},
	},
}
var (
	result [32]int
	idx    int
)

func f(v int) {
	result[idx] = v
	idx++
}
func main() {
	idx = 0
	Rekursif(t, f)
	fmt.Println("rekursif:", result[:idx])
	idx = 0
	Iteratif(t, f)
	fmt.Println("iteratif:", result[:idx])
}
